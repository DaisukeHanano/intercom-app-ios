//
//  AppDelegate.swift
//  IntercomApp-iOS
//
//  Created by 花野大助 on 2020/05/07.
//  Copyright © 2020 花野大助. All rights reserved.
//

import UIKit
import UserNotifications
import UserNotificationsUI

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
            
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { granted, error in
            guard granted else { return }

            DispatchQueue.main.async {
                // ② プッシュ通知利用の登録
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
        
        let ud = UserDefaults.standard
        if(ud.object(forKey: "Sound") == nil)
        {
            ud.set("着信音1", forKey: "Sound")
            ud.synchronize()
        }

        return true
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let token = deviceToken.map { String(format: "%.2hhx", $0) }.joined()
        print("Device token: \(token)")
        let ud = UserDefaults.standard
        ud.set(token, forKey: "deviceToken")
        ud.synchronize()
    }
        


    // MARK: UISceneSession Lifecycle

  
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("aaa")
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
       
        let notification = Notification(name: Notification.Name(rawValue: "PUSH_NOTIFICATION"))
        NotificationCenter.default.post(notification)

    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        switch application.applicationState {
        case .inactive:
            // アプリがバックグラウンドにいる状態で、Push通知から起動したとき
         
            let notification = Notification(name: Notification.Name(rawValue: "PUSH_NOTIFICATION"))
            NotificationCenter.default.post(notification)
            break
        case .active:
            // アプリ起動時にPush通知を受信したとき
            break
        case .background:
            // アプリがバックグラウンドにいる状態でPush通知を受信したとき
            break
         default:
            break
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
     
               
        let apsPart = userInfo["aps"] as? [String: AnyObject]

         
        switch application.applicationState {
        case .inactive:
            // アプリがバックグラウンドにいる状態で、Push通知から起動したとき
         
            let notification = Notification(name: Notification.Name(rawValue: "PUSH_NOTIFICATION"))
            NotificationCenter.default.post(notification)
            
            let ud = UserDefaults.standard
            ud.set(apsPart!["alert"] as! String, forKey: "ChakushinName")
            ud.synchronize()
            break
        case .active:
            let str = apsPart!["alert"] as! String
            // アプリ起動時にPush通知を受信したとき
            let ud = UserDefaults.standard
            ud.set(apsPart!["alert"] as! String, forKey: "ChakushinName")
            ud.synchronize()
            
            if(str.contains("応答しました"))
            {
                let ud = UserDefaults.standard
                if((ud.object(forKey: "isMantion") as! String ) != "1")
                {
                    let notification = Notification(name: Notification.Name(rawValue: "outouEvent"))
                    NotificationCenter.default.post(notification)
                }
                else
                {
                    let notification = Notification(name: Notification.Name(rawValue: "outouEvent"))
                    NotificationCenter.default.post(notification)
                }
                
            }
            break
        case .background:
            let str = apsPart!["alert"] as! String
            // アプリがバックグラウンドにいる状態でPush通知を受信したとき
            let ud = UserDefaults.standard
            ud.set(apsPart!["alert"] as! String, forKey: "ChakushinName")
            ud.synchronize()
            
            
            
            if(str.contains("応答しました"))
            {
                if((ud.object(forKey: "isMantion") as! String ) != "1")
                {
                    let notification = Notification(name: Notification.Name(rawValue: "outouEvent"))
                    NotificationCenter.default.post(notification)
                }
                else{
                    let notification = Notification(name: Notification.Name(rawValue: "outouEvent"))
                    NotificationCenter.default.post(notification)
                }
                
            }
            
            break
         default:
            break
        }
        
    }


}

