//
//  ViewController.swift
//  IntercomApp-iOS
//
//  Created by 花野大助 on 2020/05/07.
//  Copyright © 2020 花野大助. All rights reserved.
//

import UIKit
import SkyWay
import AVFoundation

extension ViewController: AVAudioPlayerDelegate {
    func playSound(name: String) {
        guard let path = Bundle.main.path(forResource: name, ofType: "mp3") else {
            print("音源ファイルが見つかりません")
            return
        }

        do {
            // AVAudioPlayerのインスタンス化
            audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: path))
            // AVAudioPlayerのデリゲートをセット
            audioPlayer.delegate = self
            audioPlayer.numberOfLoops = -1
            // 音声の再生
            audioPlayer.play()
        } catch {
        }
    }
}

class ViewController: UIViewController {
    @IBOutlet weak var disconnectButton: UIButton!
    @IBOutlet weak var WaitLabel: UILabel!
    @IBOutlet weak var opendoorButton: UIButton!
    @IBOutlet weak var remoteStreamView: SKWVideo!
    let kAPI_KEY = "5d26dae7-6e58-43eb-b22b-829fbe4c6cab"
    let kDOMAIN = "localhost"
    var peer : SKWPeer?
    var localStream : SKWMediaStream?
    var remoteStream : SKWMediaStream?
    var mediaConnection : SKWMediaConnection?
    var dataConnection: SKWDataConnection?
    var strOwnID = ""
    var bConnected = false
    var chakushinName = ""
    var isoutou = false
    
    var audioPlayer: AVAudioPlayer!
    var Entrance_peer_id:String = ""
    var EntranceImageURL : String = ""
    var set_history_url = "https://webrtc.nishi-dentsu.com/teleark/api/SetHistory.php"
    
    var set_mantion_history_url = "https://webrtc.nishi-dentsu.com/teleark/api/SetMantionHistory.php"
    var pushURL = "https://webrtc.nishi-dentsu.com/teleark/api/push/send_android.php"
    var pushURL_ios = "https://webrtc.nishi-dentsu.com/teleark/api/push/send_ios.php"
    
    var pushURL_Mantion = "https://webrtc.nishi-dentsu.com/teleark/api/push/send_mansion_android.php"
    var pushURL_Mantion_ios = "https://webrtc.nishi-dentsu.com/teleark/api/push/send_mansion_ios.php"
    
    
    var TelMAXTime = 90
    
    @IBOutlet weak var HistoryBtn: UIButton!
    @IBOutlet weak var EntranceCallBtn: UIButton!
    var timer : Timer? = nil
    
    var alertController:UIAlertController? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let notification = NotificationCenter.default
        notification.addObserver(self, selector: #selector(self.outouEvent), name: Notification.Name(rawValue: "outouEvent"), object: nil)
        
        self.navigationItem.hidesBackButton = true
        let ud = UserDefaults.standard
        let deviceID = ud.object(forKey: "deviceid") as! String
        
        if((ud.object(forKey: "isMantion") as! String ) == "1")
        {
            EntranceCallBtn.isHidden = true
        }
        let option : SKWPeerOption = SKWPeerOption()
        option.key = kAPI_KEY
        option.domain = kDOMAIN
        option.debug = .DEBUG_LEVEL_ALL_LOGS
        peer = SKWPeer(id: deviceID, options: option)
        setSkywayCallback()
        startLocalStream()
        // MARK: PEER_EVENT_OPEN
        
        // Do any additional setup after loading the view.
    }
    
    func outouPush_android(){
        let ud = UserDefaults.standard
        if((ud.object(forKey: "isMantion") as! String ) == "1")
        {
            
            let url = URL(string: pushURL_Mantion)
            var request = URLRequest(url: url!)
            let ud = UserDefaults.standard
            
            let user_name:String = ud.object(forKey: "user_name") as! String
            let id:String = ud.object(forKey: "id") as! String
            let property_id : String = ud.object(forKey: "ChakushinPropertyID") as! String
            let message:String = getToday() + " " + user_name + "が応答しました。"
            let room_no:String = ud.object(forKey: "room_no") as! String
            // POSTを指定
            request.httpMethod = "POST"
            // POSTするデータをBodyとして設定
            request.httpBody = ("message=" + message +  "&property=" + property_id + "&room_no=" + room_no ).data(using: .utf8)
            let session = URLSession.shared
            session.dataTask(with: request) { (data, response, error) in
                if error == nil, let data = data, let response = response as? HTTPURLResponse {
                    // HTTPヘッダの取得
                    print("Content-Type: \(response.allHeaderFields["Content-Type"] ?? "")")
                    // HTTPステータスコード
                    print("statusCode: \(response.statusCode)")
                    print(String(data: data, encoding: .utf8) ?? "")
                    if(response.statusCode == 200)
                    {
                        
                    }
                }
            }.resume()
        }
        else{
            let url = URL(string: pushURL)
            var request = URLRequest(url: url!)
            let ud = UserDefaults.standard
            
            let user_name:String = ud.object(forKey: "user_name") as! String
            let id:String = ud.object(forKey: "id") as! String
            let property_id : String = ud.object(forKey: "ChakushinPropertyID") as! String
            let message:String = getToday() + " " + user_name + "が応答しました。"
            // POSTを指定
            request.httpMethod = "POST"
            // POSTするデータをBodyとして設定
            request.httpBody = ("message=" + message +  "&property=" + property_id ).data(using: .utf8)
            let session = URLSession.shared
            session.dataTask(with: request) { (data, response, error) in
                if error == nil, let data = data, let response = response as? HTTPURLResponse {
                    // HTTPヘッダの取得
                    print("Content-Type: \(response.allHeaderFields["Content-Type"] ?? "")")
                    // HTTPステータスコード
                    print("statusCode: \(response.statusCode)")
                    print(String(data: data, encoding: .utf8) ?? "")
                    if(response.statusCode == 200)
                    {
                        
                    }
                }
            }.resume()
        }
        
    }
    
    func outouPush(){
        let ud = UserDefaults.standard
        if((ud.object(forKey: "isMantion") as! String ) == "1")
        {
            
            let url = URL(string: pushURL_Mantion_ios)
            var request = URLRequest(url: url!)
            let ud = UserDefaults.standard
            
            let user_name:String = ud.object(forKey: "user_name") as! String
            let id:String = ud.object(forKey: "id") as! String
            let property_id : String = ud.object(forKey: "ChakushinPropertyID") as! String
            let message:String = getToday() + " " + user_name + "が応答しました。"
            let room_no:String = ud.object(forKey: "room_no") as! String
            // POSTを指定
            request.httpMethod = "POST"
            // POSTするデータをBodyとして設定
            request.httpBody = ("message=" + message +  "&property=" + property_id + "&room_no=" + room_no ).data(using: .utf8)
            let session = URLSession.shared
            session.dataTask(with: request) { (data, response, error) in
                if error == nil, let data = data, let response = response as? HTTPURLResponse {
                    // HTTPヘッダの取得
                    print("Content-Type: \(response.allHeaderFields["Content-Type"] ?? "")")
                    // HTTPステータスコード
                    print("statusCode: \(response.statusCode)")
                    print(String(data: data, encoding: .utf8) ?? "")
                    if(response.statusCode == 200)
                    {
                        
                    }
                }
            }.resume()
        }
        else{
            let url = URL(string: pushURL_ios)
            var request = URLRequest(url: url!)
            let ud = UserDefaults.standard
            
            let user_name:String = ud.object(forKey: "user_name") as! String
            let id:String = ud.object(forKey: "id") as! String
            let property_id : String = ud.object(forKey: "ChakushinPropertyID") as! String
            let message:String = getToday() + " " + user_name + "が応答しました。"
            // POSTを指定
            request.httpMethod = "POST"
            // POSTするデータをBodyとして設定
            request.httpBody = ("message=" + message +  "&property=" + property_id ).data(using: .utf8)
            let session = URLSession.shared
            session.dataTask(with: request) { (data, response, error) in
                if error == nil, let data = data, let response = response as? HTTPURLResponse {
                    // HTTPヘッダの取得
                    print("Content-Type: \(response.allHeaderFields["Content-Type"] ?? "")")
                    // HTTPステータスコード
                    print("statusCode: \(response.statusCode)")
                    print(String(data: data, encoding: .utf8) ?? "")
                    if(response.statusCode == 200)
                    {
                        
                    }
                }
            }.resume()
        }
        
    }
    
    func getToday() -> String{
        let dt = Date()
        let dateFormatter = DateFormatter()

        // DateFormatter を使用して書式とロケールを指定する
        dateFormatter.dateFormat = DateFormatter.dateFormat(fromTemplate: "yMMMdHms", options: 0, locale: Locale(identifier: "ja_JP"))

        return dateFormatter.string(from: dt)
        

    }
    
    @objc func outouEvent(){
        if(self.isoutou == false)
        {
            if(self.alertController != nil)
            {
                self.alertController?.dismiss(animated: true, completion: nil)
            }
            if(self.audioPlayer != nil)
            {
                self.audioPlayer.stop()
            }
            
            self.EntranceCallBtn.isEnabled = true
            self.HistoryBtn.isEnabled = true
            EntranceCallBtn.alpha = 1.0
            HistoryBtn.alpha = 1.0
            self.disconnectButton.isHidden = true
            self.opendoorButton.isHidden = true
            
            self.opendoorButton.isEnabled = true
            
            self.WaitLabel.isHidden = false
            self.closeRemoteStream()
        
        
            DispatchQueue.main.async {
                // メインスレッドで実行 UIの処理など
                let alertController:UIAlertController =
                            UIAlertController(title:"確認",
                                      message: "他の室内機が応答しました。",
                                      preferredStyle: .alert)

                // Default のaction
//                let defaultAction:UIAlertAction =
//                            UIAlertAction(title: "OK",
//                                  style: .default,
//                                  handler:{
//                                    (action:UIAlertAction!) -> Void in
//                                    exit(0)
//                                    // 処理
//                        })
                // actionを追加
//                alertController.addAction(defaultAction)
                // UIAlertControllerの起動
                
                // アラート表示
                        self.present(alertController, animated: true, completion: {
                            // アラートを閉じる
                            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
                                exit(0)
                            })
                        })
            }
        }
        
    }
    
    func setHistoryLog(_ content:String,imageurl:String)
    {
        
        let ud = UserDefaults.standard
        let deviceID = ud.object(forKey: "deviceid") as! String
        let room_no:String = ud.object(forKey: "room_no") as! String
        if((ud.object(forKey: "isMantion") as! String ) == "1")
        {
            let url = URL(string: set_mantion_history_url)
            var request = URLRequest(url: url!)
            let ud = UserDefaults.standard
            let id:String = ud.object(forKey: "id") as! String
            let property_id : String = ud.object(forKey: "ChakushinPropertyID") as! String
            
            // POSTを指定
            request.httpMethod = "POST"
            // POSTするデータをBodyとして設定
            request.httpBody = ("user_id=" + id + "&content=" + content + "&property_id=" + property_id + "&url=" + imageurl + "&room_no=" + room_no ).data(using: .utf8)
            let session = URLSession.shared
            session.dataTask(with: request) { (data, response, error) in
                if error == nil, let data = data, let response = response as? HTTPURLResponse {
                    // HTTPヘッダの取得
                    print("Content-Type: \(response.allHeaderFields["Content-Type"] ?? "")")
                    // HTTPステータスコード
                    print("statusCode: \(response.statusCode)")
                    print(String(data: data, encoding: .utf8) ?? "")
                    if(response.statusCode == 200)
                    {
                        
                    }
                }
            }.resume()
        }
        else
        {
            let url = URL(string: set_history_url)
            var request = URLRequest(url: url!)
            let ud = UserDefaults.standard
            let id:String = ud.object(forKey: "id") as! String
            let property_id : String = ud.object(forKey: "ChakushinPropertyID") as! String
            
            // POSTを指定
            request.httpMethod = "POST"
            // POSTするデータをBodyとして設定
            request.httpBody = ("user_id=" + id + "&content=" + content + "&property_id=" + property_id + "&url=" + imageurl ).data(using: .utf8)
            let session = URLSession.shared
            session.dataTask(with: request) { (data, response, error) in
                if error == nil, let data = data, let response = response as? HTTPURLResponse {
                    // HTTPヘッダの取得
                    print("Content-Type: \(response.allHeaderFields["Content-Type"] ?? "")")
                    // HTTPステータスコード
                    print("statusCode: \(response.statusCode)")
                    print(String(data: data, encoding: .utf8) ?? "")
                    if(response.statusCode == 200)
                    {
                        
                    }
                }
            }.resume()
        }
        
    }
    
    @IBAction func SettingButtonAction(_ sender: Any) {
        
        let alertController:UIAlertController =
                    UIAlertController(title:"確認",
                              message: "選択してください",
                              preferredStyle: .alert)
         
         
        // Default のaction
        let defaultAction:UIAlertAction =
                    UIAlertAction(title: "設定",
                          style: .default,
                          handler:{
                            (action:UIAlertAction!) -> Void in
                            // 処理
                            self.performSegue(withIdentifier: "GoSetting", sender: nil)
//                            GoSetting
                })
        let defaultAction2:UIAlertAction =
            UIAlertAction(title: "自分の情報",
                  style: .default,
                  handler:{
                    (action:UIAlertAction!) -> Void in
                    self.performSegue(withIdentifier: "GoMyInfo", sender: nil)
                    // 処理
        })
        let CancelAction:UIAlertAction =
            UIAlertAction(title: "キャンセル",
                          style: .cancel,
                  handler:{
                    (action:UIAlertAction!) -> Void in
                    // 処理
        })
         
        
        // actionを追加
        alertController.addAction(defaultAction)
        alertController.addAction(defaultAction2)
        
        alertController.addAction(CancelAction)
         
        // UIAlertControllerの起動
        present(alertController, animated: true, completion: nil)
    }
    @objc func vibration(){
//        AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
        
        AudioServicesPlaySystemSoundWithCompletion(kSystemSoundID_Vibrate, {
            print("完了")
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    func startLocalStream(){
        SKWNavigator.initialize(peer!)
        var constraints = SKWMediaConstraints()
        constraints.cameraPosition = .CAMERA_POSITION_FRONT
        localStream = SKWNavigator.getUserMedia(constraints)
        localStream?.addVideoRenderer(SKWVideo(), track: 0)
        
    }

    func remoteAudioSpeaker() {
         self.remoteStream?.setEnableAudioTrack(0, enable: true)
         DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
             let audioSession = AVAudioSession.sharedInstance()
             do {
                 try audioSession.overrideOutputAudioPort(AVAudioSession.PortOverride.speaker)
             } catch let error as NSError {
                 print("audioSession error: \(error.localizedDescription)")
             }
         }
     }
    
    func showChakushinAlert(){
        
        let ud = UserDefaults.standard
        
        
        
        
        
        
        DispatchQueue.main.async {
            // メインスレッドで実行 UIの処理など
            self.alertController =
                        UIAlertController(title:"確認",
                                  message: ud.object(forKey: "ChakushinName") as! String,
                                  preferredStyle: .alert)

            
            // Default のaction
            let defaultAction:UIAlertAction =
                        UIAlertAction(title: "応答",
                              style: .default,
                              handler:{
                                (action:UIAlertAction!) -> Void in
                                // 処理
                                self.isoutou = true
                                self.localStream?.setEnableAudioTrack(0, enable: true)
                                self.bConnected = true
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                    // your code here
                                    if((ud.object(forKey: "isMantion") as! String ) != "1")
                                    {
                                        self.outouPush()
                                        self.outouPush_android()
                                    }
                                    else
                                    {
                                        self.outouPush()
                                        self.outouPush_android()
                                    }
                                    
                                }
                                
                                
                                var str = "応答"
                                self.dataConnection?.send(str as NSObject)
                                
                                self.WaitLabel.isHidden = true
                                self.opendoorButton.isHidden = false
                                self.disconnectButton.isHidden = false
                               
//                                if(self.timer?.isValid == true)
//                                {
//                                    self.timer?.invalidate()
//                                }
                                
                                self.audioPlayer.stop()
                                
                                var delay = self.TelMAXTime/1000
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + Double(delay)) {
                                    
                                    if(self.bConnected == false)
                                    {
                                        var str = "最大通話時間超過"
                                        self.dataConnection?.send(str as NSObject)
                                        self.closeRemoteStream()
                                    }
                                    
                                    print("90秒後の処理")
                                }
                    })
            
            // Default のaction
            let defaultAction2:UIAlertAction =
                        UIAlertAction(title: "拒否",
                              style: .default,
                              handler:{
                                (action:UIAlertAction!) -> Void in
                                // 処理
                                
                                
                                let alertController:UIAlertController =
                                            UIAlertController(title:"確認",
                                                      message: "本当に拒否しますか？",
                                                      preferredStyle: .alert)
                                
                                let defaultAction:UIAlertAction =
                                            UIAlertAction(title: "はい",
                                                  style: .default,
                                                  handler:{
                                                    (action:UIAlertAction!) -> Void in
                                                    // 処理
                                                    self.isoutou = false
                                                    self.EntranceCallBtn.isEnabled = true
                                                    self.HistoryBtn.isEnabled = true
                                                    self.EntranceCallBtn.alpha = 1.0
                                                    self.HistoryBtn.alpha = 1.0
                                                    self.localStream?.setEnableAudioTrack(0, enable: false)
                                                    self.bConnected = true
//                                                    if(self.timer?.isValid == true)
//                                                    {
//                                                        self.timer?.invalidate()
//                                                    }
                                                    self.audioPlayer.stop()
                                                    var str = "拒否"
                                                                
                                                    self.dataConnection?.send(str as NSObject)
                                                    self.setHistoryLog("着信を拒否しました", imageurl: self.EntranceImageURL)
                                                    self.closeRemoteStream()
                                                    exit(0)
                                                    
                                        
                                            })
                                let defaultAction2:UIAlertAction =
                                    UIAlertAction(title: "いいえ",
                                          style: .default,
                                          handler:{
                                            (action:UIAlertAction!) -> Void in
                                            // 処理
                                            self.showChakushinAlert()
                                    })
                                alertController.addAction(defaultAction)
                                alertController.addAction(defaultAction2)
                                self.present(alertController, animated: true, completion: nil)
                    })
            // actionを追加
            self.alertController!.addAction(defaultAction)
            self.alertController!.addAction(defaultAction2)
            // UIAlertControllerの起動
            self.present(self.alertController!, animated: true, completion: nil)
        }
    }
    @IBAction func historyButtonTapped(_ sender: Any) {
        //着信履歴ボタンタップ時
        performSegue(withIdentifier: "GoHistory", sender: nil)
    }
    
    @IBAction func CallButtonTapped(_ sender: Any) {
        //玄関機呼出し
        performSegue(withIdentifier: "GoCallEntrance", sender: nil)
    }
    
    
    func setSkywayCallback(){
        peer?.on(.PEER_EVENT_OPEN,callback:{ (obj) -> Void in
            if let peerId = obj as? String{
                DispatchQueue.main.async {
                    print(peerId)
                }
                let constraints = SKWMediaConstraints()
                constraints.cameraPosition = .CAMERA_POSITION_FRONT
                SKWNavigator.initialize(self.peer!)
               
                print("your peerId: \(peerId)")
            }
        })
        
        peer?.on(.PEER_EVENT_CALL,callback:{ (obj) -> Void in
            
            if(obj is SKWMediaConnection)
            {
                
                self.EntranceCallBtn.isEnabled = false
                self.HistoryBtn.isEnabled = false
                self.EntranceCallBtn.alpha = 0.5
                self.HistoryBtn.alpha = 0.5
                
                self.setupMediaConnectionCallbacks(mediaConnection: (obj as! SKWMediaConnection))
                self.mediaConnection = (obj as! SKWMediaConnection)
                self.localStream?.setEnableAudioTrack(0, enable: false)
                self.remoteStream?.setEnableAudioTrack(0, enable: false)
                self.remoteAudioSpeaker()
                self.mediaConnection?.answer()
                
                
                let ud = UserDefaults.standard
                
                
                DispatchQueue.main.async {
                    
                    
                    if((ud.object(forKey: "Sound") as! String) == "着信音1")
                    {
                        self.playSound(name: "sound1")
                    }
                    else if((ud.object(forKey: "Sound") as! String) == "着信音2")
                    {
                        self.playSound(name: "sound2")
                    }
                    else if((ud.object(forKey: "Sound") as! String) == "着信音3")
                    {
                        self.playSound(name: "sound3")
                    }
                    else if((ud.object(forKey: "Sound") as! String) == "着信音4")
                    {
                        self.playSound(name: "sound4")
                    }
                    else if((ud.object(forKey: "Sound") as! String) == "着信音5")
                    {
                        self.playSound(name: "sound5")
                    }
                    var text = ud.object(forKey: "ChakushinName") as! String
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 10.0) { [self] in
                        // your code here
                        self.setHistoryLog(text, imageurl: self.EntranceImageURL)
                    }
//
//                    self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.vibration), userInfo: nil, repeats: true)
                    // メインスレッドで実行 UIの処理など
                    self.alertController =
                                UIAlertController(title:"確認",
                                                  message: ud.object(forKey: "ChakushinName") as! String,
                                          preferredStyle: .alert)

                    // Default のaction
                    let defaultAction:UIAlertAction =
                                UIAlertAction(title: "応答",
                                      style: .default,
                                      handler:{
                                        (action:UIAlertAction!) -> Void in
                                        // 処理
                                    
                                        self.isoutou = true
                                        self.localStream?.setEnableAudioTrack(0, enable: true)
                                        self.bConnected = true
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                                        self.mediaConnection?.replace(self.localStream)
                                        self.remoteStream?.setEnableAudioTrack(0, enable: true)
                                    }
                                    
                                        var str = "応答"
                                        self.dataConnection?.send(str as NSObject)

                                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                            // your code here
                                            if((ud.object(forKey: "isMantion") as! String ) != "1")
                                            {
                                                self.outouPush()
                                                self.outouPush_android()
                                            }
                                            else
                                            {
                                                self.outouPush()
                                                self.outouPush_android()
                                            }
                                            
                                        }
                                        
                                        self.WaitLabel.isHidden = true
                                        self.opendoorButton.isHidden = false
                                        self.disconnectButton.isHidden = false
                                        
                                        self.audioPlayer.stop()
                                        var delay = self.TelMAXTime/1000
                                        DispatchQueue.main.asyncAfter(deadline: .now() + Double(delay)) {
                                            
                                            if(self.bConnected == false)
                                            {
                                                var str = "最大通話時間超過"
                                                self.dataConnection?.send(str as NSObject)
                                                self.closeRemoteStream()
                                            }
                                            
                                            print("90秒後の処理")
                                        }
                            })
                    
                    // Default のaction
                    let defaultAction2:UIAlertAction =
                                UIAlertAction(title: "拒否",
                                      style: .default,
                                      handler:{
                                        (action:UIAlertAction!) -> Void in
                                        // 処理
                                        
                                        
                                        let alertController:UIAlertController =
                                                    UIAlertController(title:"確認",
                                                              message: "本当に拒否しますか？",
                                                              preferredStyle: .alert)
                                        
                                        let defaultAction:UIAlertAction =
                                                    UIAlertAction(title: "はい",
                                                          style: .default,
                                                          handler:{
                                                            (action:UIAlertAction!) -> Void in
                                                            // 処理
                                                            self.isoutou = false
                                                            self.EntranceCallBtn.isEnabled = true
                                                            self.HistoryBtn.isEnabled = true
                                                            self.EntranceCallBtn.alpha = 1.0
                                                            self.HistoryBtn.alpha = 1.0
                                                            self.localStream?.setEnableAudioTrack(0, enable: false)
                                                            self.bConnected = true
                                                            var str = "拒否"
                                                                  
//                                                            if(self.timer?.isValid == true)
//                                                            {
//                                                                self.timer?.invalidate()
//                                                            }
                                                            self.audioPlayer.stop()
                                                            self.dataConnection?.send(str as NSObject)
                                                            
                                                            self.setHistoryLog("着信を拒否しました", imageurl: self.EntranceImageURL)
                                                            self.closeRemoteStream()
                                                            exit(0)
                                                
                                                    })
                                        let defaultAction2:UIAlertAction =
                                            UIAlertAction(title: "いいえ",
                                                  style: .default,
                                                  handler:{
                                                    (action:UIAlertAction!) -> Void in
                                                    // 処理
                                                    self.showChakushinAlert()
                                            })
                                        alertController.addAction(defaultAction)
                                        alertController.addAction(defaultAction2)
                                        self.present(alertController, animated: true, completion: nil)
                            })
                    // actionを追加
                    self.alertController!.addAction(defaultAction)
                    self.alertController!.addAction(defaultAction2)
                    // UIAlertControllerの起動
                    self.present(self.alertController!, animated: true, completion: nil)
                }
                
                
            }
            else{
                return
            }
        })
        
        peer?.on(.PEER_EVENT_CONNECTION,callback:{ (obj) -> Void in
            
            if(obj is SKWDataConnection)
            {
                self.dataConnection = (obj as! SKWDataConnection)
                self.setDataCallbacks()
            }
        })
    }
    
    func setupMediaConnectionCallbacks(mediaConnection:SKWMediaConnection){
        
        // MARK: MEDIACONNECTION_EVENT_STREAM
        mediaConnection.on(SKWMediaConnectionEventEnum.MEDIACONNECTION_EVENT_STREAM, callback: { (obj) -> Void in
            
            if let msStream = obj as? SKWMediaStream{
                self.remoteStream = msStream
                DispatchQueue.main.async {
                    self.remoteStream?.addVideoRenderer(self.remoteStreamView, track: 0)
                }
            }
        })
        
        // MARK: MEDIACONNECTION_EVENT_CLOSE
        mediaConnection.on(SKWMediaConnectionEventEnum.MEDIACONNECTION_EVENT_CLOSE, callback: { (obj) -> Void in
            if let _ = obj as? SKWMediaConnection{
                DispatchQueue.main.async {
                    self.remoteStream?.removeVideoRenderer(self.remoteStreamView, track: 0)
                    self.remoteStream = nil
                    self.mediaConnection = nil
                }
            }
        })
    }
    
    
    @IBAction func disconnectButtonTapped(_ sender: Any) {
        if(dataConnection != nil)
        {
            localStream?.setEnableAudioTrack(0, enable: false)
            let str = "切断"
            self.isoutou = false
            self.setHistoryLog("切断しました", imageurl: self.EntranceImageURL)
            dataConnection?.send(str as NSObject)
            self.EntranceCallBtn.isEnabled = true
            self.HistoryBtn.isEnabled = true
            EntranceCallBtn.alpha = 1.0
            HistoryBtn.alpha = 1.0
            self.disconnectButton.isHidden = true
            self.opendoorButton.isHidden = true
            
            self.opendoorButton.isEnabled = true
            
            self.WaitLabel.isHidden = true
            
            DispatchQueue.main.async {
                // メインスレッドで実行 UIの処理など
                let alertController:UIAlertController =
                            UIAlertController(title:"確認",
                                      message: "切断しました。",
                                      preferredStyle: .alert)

                // Default のaction
                let defaultAction:UIAlertAction =
                            UIAlertAction(title: "OK",
                                  style: .default,
                                  handler:{
                                    (action:UIAlertAction!) -> Void in
                                    // 処理
                                    self.closeRemoteStream()
                                    
                                    exit(0)
                        })
                // actionを追加
                alertController.addAction(defaultAction)
                // UIAlertControllerの起動
                self.present(alertController, animated: true, completion: nil)
            }
        }
        else{
            DispatchQueue.main.async {
                // メインスレッドで実行 UIの処理など
                let alertController:UIAlertController =
                            UIAlertController(title:"確認",
                                      message: "玄関機に接続されていないため切断できません。",
                                      preferredStyle: .alert)

                // Default のaction
                let defaultAction:UIAlertAction =
                            UIAlertAction(title: "OK",
                                  style: .default,
                                  handler:{
                                    (action:UIAlertAction!) -> Void in
                                    // 処理
                        })
                // actionを追加
                alertController.addAction(defaultAction)
                // UIAlertControllerの起動
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    func closeRemoteStream(){
        if(remoteStream == nil){
            return
        }
        
        if(remoteStreamView != nil)
        {
            remoteStream?.removeVideoRenderer(remoteStreamView, track: 0)
        }
        bConnected = false
        remoteStream?.close()
        dataConnection?.close()
        remoteStream = nil
        dataConnection = nil
    }
    
    func setDataCallbacks(){
        dataConnection!.on(SKWDataConnectionEventEnum.DATACONNECTION_EVENT_OPEN, callback: { (obj) -> Void in
            self.bConnected = true
        })
        
        dataConnection!.on(SKWDataConnectionEventEnum.DATACONNECTION_EVENT_CLOSE, callback: { (obj) -> Void in
            self.bConnected = false
        })
        
        dataConnection!.on(SKWDataConnectionEventEnum.DATACONNECTION_EVENT_DATA, callback: { [self] (obj) -> Void in
            
            
            var str = ""
            if(obj is String)
            {
                str = obj as! String
            }
            
            if str.contains("物件名")
            {
                self.chakushinName = str
            }
            else if str.contains("呼び出しキャンセル")
            {
                if(self.alertController != nil)
                {
                    self.alertController?.dismiss(animated: true, completion: nil)
                }
                self.audioPlayer.stop()
                self.EntranceCallBtn.isEnabled = true
                self.HistoryBtn.isEnabled = true
                EntranceCallBtn.alpha = 1.0
                HistoryBtn.alpha = 1.0
                self.disconnectButton.isHidden = true
                self.opendoorButton.isHidden = true
                
                self.opendoorButton.isEnabled = true
                
                self.WaitLabel.isHidden = true
                self.closeRemoteStream()
                
                DispatchQueue.main.async {
                    // メインスレッドで実行 UIの処理など
                    let alertController:UIAlertController =
                                UIAlertController(title:"確認",
                                          message: "呼び出しがキャンセルされました。",
                                          preferredStyle: .alert)

                    // Default のaction
                    let defaultAction:UIAlertAction =
                                UIAlertAction(title: "OK",
                                      style: .default,
                                      handler:{
                                        (action:UIAlertAction!) -> Void in
                                        exit(0)
                                        // 処理
                            })
                    // actionを追加
                    alertController.addAction(defaultAction)
                    // UIAlertControllerの起動
                    self.present(alertController, animated: true, completion: nil)
                }
                
            }
            else if str.contains("マンション")
            {
                
            }
            else if str.contains("最大通話時間")
            {
                let arr = str.split(separator: ":")
                TelMAXTime = Int(arr[1])!
            }
            else if str.contains("物件ID")
            {
                let arr = str.split(separator: ":")
                let id = arr[1]
                let ud = UserDefaults.standard
                ud.setValue(id, forKey: "ChakushinPropertyID")
                ud.synchronize()
            }
            else if str.contains("トイレモード")
            {
                self.opendoorButton.isEnabled = false
                remoteStreamView.isHidden = true
            }
            else if str.contains("玄関機名")
            {
                
            }
            else if str.contains("玄関機画像")
            {
                let arr = str.split(separator: ",")
                EntranceImageURL = String(arr[1])
            }
        })
        dataConnection!.on(SKWDataConnectionEventEnum.DATACONNECTION_EVENT_ERROR, callback: { (obj) -> Void in
        })
        
    }
    
    
    @IBAction func doorOpenButtonTapped(_ sender: Any) {
        let str = "解錠"
        if(dataConnection != nil)
        {
            self.isoutou = false
            self.setHistoryLog("応答し解錠しました", imageurl: self.EntranceImageURL)
            
            dataConnection?.send(str as NSObject)
            
            localStream?.setEnableAudioTrack(0, enable: false)
            self.EntranceCallBtn.isEnabled = true
            self.HistoryBtn.isEnabled = true
            EntranceCallBtn.alpha = 1.0
            HistoryBtn.alpha = 1.0
            self.disconnectButton.isHidden = true
            self.opendoorButton.isHidden = true
            self.opendoorButton.isEnabled = true
            self.WaitLabel.isHidden = true
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                self.closeRemoteStream()
            }
            DispatchQueue.main.async {
                // メインスレッドで実行 UIの処理など
                let alertController:UIAlertController =
                            UIAlertController(title:"確認",
                                      message: "解錠しました。",
                                      preferredStyle: .alert)

                // Default のaction
                let defaultAction:UIAlertAction =
                            UIAlertAction(title: "OK",
                                  style: .default,
                                  handler:{
                                    (action:UIAlertAction!) -> Void in
                                    // 処理
                                    exit(0)
                                    
                        })
                // actionを追加
                alertController.addAction(defaultAction)
                // UIAlertControllerの起動
                self.present(alertController, animated: true, completion: nil)
            }
        }
        else{
            DispatchQueue.main.async {
                // メインスレッドで実行 UIの処理など
                let alertController:UIAlertController =
                            UIAlertController(title:"確認",
                                      message: "玄関機と接続されていないため解錠できません。",
                                      preferredStyle: .alert)

                // Default のaction
                let defaultAction:UIAlertAction =
                            UIAlertAction(title: "OK",
                                  style: .default,
                                  handler:{
                                    (action:UIAlertAction!) -> Void in
                                    // 処理
                        })
                // actionを追加
                alertController.addAction(defaultAction)
                // UIAlertControllerの起動
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    func call(peer_id:String)
    {
         localStream?.setEnableAudioTrack(0, enable: true)
        dataConnection
            = peer?.connect(withId: peer_id)
        let option = SKWCallOption()
        mediaConnection = peer?.call(withId: peer_id, stream: localStream, options: option)
        self.setupMediaConnectionCallbacks(mediaConnection: mediaConnection!)
        EntranceCallBtn.isEnabled = false
        HistoryBtn.isEnabled = false
        EntranceCallBtn.alpha = 0.5
        HistoryBtn.alpha = 0.5
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            
            self.EntranceCallBtn.isEnabled = false
            self.HistoryBtn.isEnabled = false
            self.EntranceCallBtn.alpha = 0.5
            self.HistoryBtn.alpha = 0.5
            self.dataConnection?.send("応答" as NSObject)
            self.WaitLabel.isHidden = true
            self.opendoorButton.isHidden = false
            self.disconnectButton.isHidden = false
            self.bConnected = true
            
        }
        
    }
    
    
    

}

