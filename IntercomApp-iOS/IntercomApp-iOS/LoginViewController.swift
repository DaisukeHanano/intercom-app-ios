//
//  LoginViewController.swift
//  IntercomApp-iOS
//
//  Created by 花野大助 on 2020/06/06.
//  Copyright © 2020 花野大助. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

extension UITextField {
    func addBorderBottom(height: CGFloat, color: UIColor) {
        let border = CALayer()
        border.frame = CGRect(x: 0, y: self.frame.height - height, width: self.frame.width, height: height)
        border.backgroundColor = color.cgColor
        self.layer.addSublayer(border)
    }
}

class LoginViewController: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var IDTextField: UITextField!
    
    
    @IBOutlet weak var PassTextField: UITextField!
    
    
    func setDeviceID(_ deviceID:String,deviceToken:String)
    {
        let url = URL(string: "https://webrtc.nishi-dentsu.com/teleark/api/SetDeviceID.php")
        var request = URLRequest(url: url!)
        // POSTを指定
        request.httpMethod = "POST"
        // POSTするデータをBodyとして設定
        request.httpBody = ("id=" + IDTextField.text! + "&pass=" + PassTextField.text! + "&device_id=" + deviceID + "&device_token=" + deviceToken ).data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if error == nil, let data = data, let response = response as? HTTPURLResponse {
                // HTTPヘッダの取得
                print("Content-Type: \(response.allHeaderFields["Content-Type"] ?? "")")
                // HTTPステータスコード
                print("statusCode: \(response.statusCode)")
                print(String(data: data, encoding: .utf8) ?? "")
                if(response.statusCode == 200)
                {
                    
                }
            }
        }.resume()
    }
    
    @IBAction func LoginTapAction(_ sender: Any) {
 
        
        let url = URL(string: "https://webrtc.nishi-dentsu.com/teleark/api/CheckUser.php")
        var request = URLRequest(url: url!)
        // POSTを指定
        request.httpMethod = "POST"
        // POSTするデータをBodyとして設定
        request.httpBody = ("id=" + IDTextField.text! + "&pass=" + PassTextField.text!).data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if error == nil, let data = data, let response = response as? HTTPURLResponse {
                // HTTPヘッダの取得
                print("Content-Type: \(response.allHeaderFields["Content-Type"] ?? "")")
                // HTTPステータスコード
                print("statusCode: \(response.statusCode)")
                print(String(data: data, encoding: .utf8) ?? "")
                if(response.statusCode == 200)
                {
                    do {
                        let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! NSDictionary
                        print(json)
                        if((json["status"] as! Int ) == 0)
                        {
                            
                            var deviceID = ""
//                            deviceID = UIDevice().identifierForVendor?.uuidString as! String
                            deviceID = KeychainManager.sharedInstance.getDeviceIdentifierFromKeychain()
                            
                            let ud = UserDefaults.standard
                            ud.set(deviceID, forKey: "deviceid")
                            ud.synchronize()
                            
                            
                            var token = ""
                            if( ud.object(forKey: "deviceToken") != nil)
                            {
                                token = ud.object(forKey: "deviceToken") as! String
                            }
                            
                            print("ログイン成功")
                            if((json["device_id"] as! String) == "")
                            {
                                self.setDeviceID(deviceID, deviceToken:token)
                                DispatchQueue.main.async {
                                    let alertController:UIAlertController =
                                                           UIAlertController(title:"ようこそ!",
                                                                     message: "建物名" +  (json["name"] as! String) + "\n部屋" + (json["room_no"] as! String),
                                                                     preferredStyle: .alert)


                                               // Default のaction
                                               let defaultAction:UIAlertAction =
                                                           UIAlertAction(title: "OK",
                                                                 style: .default,
                                                                 handler:{
                                                                   (action:UIAlertAction!) -> Void in
                                                                   // 処理
                                                                    ud.set(true, forKey: "isLogin")
                                                                    ud.set((json["name"] as! String) , forKey: "buildingName")
                                                                    ud.set((json["user_name"] as! String) , forKey: "user_name")
                                                                    ud.set((json["room_no"] as! String) , forKey: "room_no")
                                                                    ud.set((json["user_id"] as! String) , forKey: "id")
                                                                    ud.set((json["tel"] as! String) , forKey: "tel")
                                                                    ud.set((json["isMantion"] as! String) , forKey: "isMantion")
                                                                    ud.synchronize()
                                                                    self.performSegue(withIdentifier: "GoTop", sender: nil)
                                                       })
                                    

                                    

                                               // actionを追加
                                               alertController.addAction(defaultAction)

                                               
                                               // UIAlertControllerの起動
                                               self.present(alertController, animated: true, completion: nil)
                                }
                                
                            }
                            else{
                                if((json["device_id"] as! String) != deviceID)
                                {
                                    
                                    DispatchQueue.main.async {
                                        // メインスレッドで実行 UIの処理など
                                        let alertController:UIAlertController =
                                                    UIAlertController(title:"確認",
                                                              message: "このIDは他の端末で登録されています。",
                                                              preferredStyle: .alert)


                                        // Default のaction
                                        let defaultAction:UIAlertAction =
                                                    UIAlertAction(title: "OK",
                                                          style: .default,
                                                          handler:{
                                                            (action:UIAlertAction!) -> Void in
                                                            // 処理
                                                           
                                                })
                                        // actionを追加
                                        alertController.addAction(defaultAction)

                                        // UIAlertControllerの起動
                                        self.present(alertController, animated: true, completion: nil)
                                    }
                                    
                                }
                                else{
                                    self.setDeviceID(deviceID, deviceToken: token)
                                    DispatchQueue.main.async {
                                        let alertController:UIAlertController =
                                                               UIAlertController(title:"ようこそ!",
                                                                         message: "建物名" +  (json["name"] as! String) + "\n部屋" + (json["room_no"] as! String),
                                                                         preferredStyle: .alert)


                                                   // Default のaction
                                                   let defaultAction:UIAlertAction =
                                                               UIAlertAction(title: "OK",
                                                                     style: .default,
                                                                     handler:{
                                                                       (action:UIAlertAction!) -> Void in
                                                                       // 処理
                                                                        ud.set(true, forKey: "isLogin")
                                                                        ud.set((json["name"] as! String) , forKey: "buildingName")
                                                                        ud.set((json["user_name"] as! String) , forKey: "user_name")
                                                                        ud.set((json["room_no"] as! String) , forKey: "room_no")
                                                                        ud.set((json["user_id"] as! String) , forKey: "id")
                                                                        ud.set((json["tel"] as! String) , forKey: "tel")
                                                                        ud.set((json["isMantion"] as! String) , forKey: "isMantion")
                                                                        ud.synchronize()
                                                                        self.performSegue(withIdentifier: "GoTop", sender: nil)
                                                           })

                                        

                                                   // actionを追加
                                                   alertController.addAction(defaultAction)

                                                   
                                                   // UIAlertControllerの起動
                                                   self.present(alertController, animated: true, completion: nil)
                                    }
                                }
                                
                            }
                        }else {
                            print("ログイン失敗")
                            DispatchQueue.main.async {
                                // メインスレッドで実行 UIの処理など
                                let alertController:UIAlertController =
                                            UIAlertController(title:"エラー",
                                                      message: "利用者IDまたはパスワードが間違っています。",
                                                      preferredStyle: .alert)


                                // Default のaction
                                let defaultAction:UIAlertAction =
                                            UIAlertAction(title: "OK",
                                                  style: .default,
                                                  handler:{
                                                    (action:UIAlertAction!) -> Void in
                                                    // 処理
                                                   
                                        })
                                // actionを追加
                                alertController.addAction(defaultAction)

                                // UIAlertControllerの起動
                                self.present(alertController, animated: true, completion: nil)
                            }
                        }
                    }
                    catch
                    {
                        
                    }
                }
                
                
                
                
            }
        }.resume()
            

    }
    
    
    @objc func keyboardHide() {
        IDTextField.resignFirstResponder()
        PassTextField.resignFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {

    }
    
    
    
    
    @objc func keyboardWillBeShown(notification: NSNotification) {
        let rect = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        let duration: TimeInterval? = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double
        UIView.animate(withDuration: duration!) {
          self.view.transform = CGAffineTransform(translationX: 0, y: -(rect?.size.height)!)
        }
    }

    @objc func keyboardWillBeHidden(notification: NSNotification) {
        let duration: TimeInterval? = notification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? Double
        UIView.animate(withDuration: duration!) {
          self.view.transform = CGAffineTransform.identity
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(self.keyboardHide))
        
        
        NotificationCenter.default.addObserver(self,
                                               selector:#selector(keyboardWillBeShown(notification:)),
            name: UIResponder.keyboardWillShowNotification,
            object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillBeHidden(notification:)),
            name: UIResponder.keyboardWillHideNotification,
            object: nil)
        
        self.view.addGestureRecognizer(tapgesture)
        IDTextField.delegate = self
        PassTextField.delegate = self
        
        IDTextField.placeholder = "IDを入力してください"
//        IDTextField.addBorderBottom(height: 1.0, color: UIColor.lightGray)
        IDTextField.addBorderBottom(height: 3.0, color: UIColor.lightGray)
        PassTextField.placeholder = "パスワードを入力してください"
        PassTextField.addBorderBottom(height: 3.0, color: UIColor.lightGray)
        // Do any additional setup after loading the view.
        
        let ud = UserDefaults.standard
        if(ud.bool(forKey: "isLogin"))
        {
            self.performSegue(withIdentifier: "GoTop", sender: nil)
        }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        IDTextField.resignFirstResponder()
        PassTextField.resignFirstResponder()
        
        return true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
