//
//  HistoryTableViewController.swift
//  IntercomApp-iOS
//
//  Created by 花野大助 on 2020/09/04.
//  Copyright © 2020 花野大助. All rights reserved.
//

import UIKit
import SDWebImage

class HistoryTableViewController: UITableViewController {

    
    let get_property_list_url = "https://webrtc.nishi-dentsu.com/teleark/api/GetPropertyList.php"
    var propertyID = ""
    var property_title = ""
    
    var histories : Array<NSDictionary> = Array()
    override func viewDidLoad() {
        super.viewDidLoad()
        getHistory()
        self.title = "履歴"
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    func getHistory(){
        let url = URL(string: get_property_list_url)
        var request = URLRequest(url: url!)
        // POSTを指定
        request.httpMethod = "POST"
        let ud = UserDefaults.standard
        let user_id = ud.object(forKey: "id") as! String
        request.httpBody = ("user_id=" + user_id ).data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if error == nil, let data = data, let response = response as? HTTPURLResponse {
                // HTTPヘッダの取得
                print("Content-Type: \(response.allHeaderFields["Content-Type"] ?? "")")
                // HTTPステータスコード
                print("statusCode: \(response.statusCode)")
                print(String(data: data, encoding: .utf8) ?? "")
                if(response.statusCode == 200)
                {
                    do {
                        let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? Array<NSDictionary>
                        print(json)
                        if(json != nil)
                        {
                            self.histories = json!
                            DispatchQueue.main.async {
                                // メインスレッドで実行 UIの処理など
                                self.tableView.reloadData()
                            }
                        }
                        
                        
                    }
                    catch
                    {
                        
                    }
                }
                
                
                
                
            }
        }.resume()

    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return histories.count
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        propertyID = histories[indexPath.row]["id"] as! String
        property_title = histories[indexPath.row]["name"] as! String
        self.performSegue(withIdentifier: "GoHistoryDetail", sender: nil)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "GoHistoryDetail")
        {
            let vc = segue.destination as! HistoryDetailTableViewController
            vc.propertyID = self.propertyID
            vc.property_title = self.property_title
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        let history = histories[indexPath.row]
        
        cell.textLabel?.text = history["name"] as! String
        // Configure the cell...

        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
