//
//  HistoryDetailTableViewCell.swift
//  IntercomApp-iOS
//
//  Created by 花野大助 on 2020/09/04.
//  Copyright © 2020 花野大助. All rights reserved.
//

import UIKit

class HistoryDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var HistoryImageView: UIImageView!
    @IBOutlet weak var DateLabel: UILabel!
    
    @IBOutlet weak var ContentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
