//
//  HistoryDetailTableViewController.swift
//  IntercomApp-iOS
//
//  Created by 花野大助 on 2020/09/04.
//  Copyright © 2020 花野大助. All rights reserved.
//

import UIKit
import SDWebImage

class HistoryDetailTableViewController: UITableViewController {

    
    var get_history_url = "https://webrtc.nishi-dentsu.com/teleark/api/GetHistory.php"
    var get_mation_history_url = "https://webrtc.nishi-dentsu.com/teleark/api/GetMantionHistory.php"
    var histories:Array<NSDictionary> = Array()
    var propertyID = ""
    var property_title = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = property_title
        
        let ud = UserDefaults.standard
      
        if((ud.object(forKey: "isMantion") as! String ) == "1")
        {
            getMantionHistoryList()
        }
        else
        {
            getHistoryList()
        }
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    func getHistoryList(){
        let url = URL(string: get_history_url)
        var request = URLRequest(url: url!)
        // POSTを指定
        request.httpMethod = "POST"
        let ud = UserDefaults.standard
        let user_id = ud.object(forKey: "id") as! String
        request.httpBody = ("user_id=" + user_id + "&property_id=" + propertyID).data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if error == nil, let data = data, let response = response as? HTTPURLResponse {
                // HTTPヘッダの取得
                print("Content-Type: \(response.allHeaderFields["Content-Type"] ?? "")")
                // HTTPステータスコード
                print("statusCode: \(response.statusCode)")
                print(String(data: data, encoding: .utf8) ?? "")
                if(response.statusCode == 200)
                {
                    do {
                        let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? Array<NSDictionary>
                        print(json)
                        if(json != nil)
                        {
                            self.histories = json!
                            
                            DispatchQueue.main.async {
                                // メインスレッドで実行 UIの処理など
                                self.tableView.reloadData()
                            }
                        }
                        
                        
                        
                    }
                    catch
                    {
                        
                    }
                }
                
                
                
                
            }
        }.resume()

    }
    
    func getMantionHistoryList(){
        let url = URL(string: get_mation_history_url)
        var request = URLRequest(url: url!)
        // POSTを指定
        request.httpMethod = "POST"
        let ud = UserDefaults.standard
        let user_id = ud.object(forKey: "id") as! String
        let room_no:String = ud.object(forKey: "room_no") as! String
        request.httpBody = ("user_id=" + user_id + "&property_id=" + propertyID + "&room_no=" + room_no).data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if error == nil, let data = data, let response = response as? HTTPURLResponse {
                // HTTPヘッダの取得
                print("Content-Type: \(response.allHeaderFields["Content-Type"] ?? "")")
                // HTTPステータスコード
                print("statusCode: \(response.statusCode)")
                print(String(data: data, encoding: .utf8) ?? "")
                if(response.statusCode == 200)
                {
                    do {
                        let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? Array<NSDictionary>
                        print(json)
                        if(json != nil)
                        {
                            self.histories = json!
                            
                            DispatchQueue.main.async {
                                // メインスレッドで実行 UIの処理など
                                self.tableView.reloadData()
                            }
                        }
                        
                        
                        
                    }
                    catch
                    {
                        
                    }
                }
                
                
                
                
            }
        }.resume()

    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return histories.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let history = histories[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! HistoryDetailTableViewCell

        cell.HistoryImageView.sd_setImage(with: URL(string: history["image_url"] as! String), completed: nil)
        cell.ContentLabel.text = history["content"] as! String
        cell.DateLabel.text = history["update_date"] as! String
        // Configure the cell...

        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
